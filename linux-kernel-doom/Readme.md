
# Introduction

I am a bundle for Doom Emacs for Linux Kernel Development on Fedora Linux with GNOME and Wayland.

Started by John B. Wyatt IV.
Red Hat Software Engineer, Core Kernel: Real-Time, Accelerators, and Trading (krats) Team.

# Setup

* Install the necessary packages for your Fedora install

*To be written later*

Do not install rust directly thorough the packages. Remove it if you have as it can cause freezing issues with Doom Emacs. It must be installed by `rustup-init`.

```
rustup-init
```

*to be written later*

Install rust-analyzer for LSP functionality. This is installed user wide.

```
rustup component add rust-analyzer
```

* Install Doom Emacs

[Install Doom Emacs](https://github.com/doomemacs/doomemacs?tab=readme-ov-file#install)

* Install this bundle

Please clone me to a folder that is *not* `~/.doom.d` or a default emacs directory. Run `setup.sh` to copy this folder to `~/.doom.d`.

Note: `setup.sh` will delete your `~/.emacs.d` and other emacs config folders.

```
./setup.sh
```

* Sync the Doom Emacs bundle

```
~/.config/emacs/bin/doom sync
```

Note: you will need to do this every time you update this repository.

For those of you who use my [Neo-ELK (for Neovim) config](https://gitlab.com/jbwyatt4/vim-megarepo/-/tree/main/neo-elk?ref_type=heads) note that I am using a script with Emacs instead of symlinking directly to a folder in the repo. This is because Emacs gets weird with a folder symlinked in the config. With Neovim it is a one time, painless operation. I may transition this to just using the root for each config in a repo for my Emacs configs if I can not figure out a way to deal with this in the future.

* Icons/Symbols in Doom Emacs (optional)

Please make sure to install the [Symbols Only Nerd Font](https://www.nerdfonts.com/font-downloads) by copying the .ttf to ~/.fonts on Fedora. Please create the folder if it does not exist.

Your system may require a reboot to make use of it.

* Setup a terminal only alias

Add this to your Bash config if you prefer to use Emacs in a terminal like I do (and for tmux).

```
# In your Bash config
alias e='emacs -nw'
```

Run from your terminal.

```
e .
```

# Troubleshooting

* terminal Emacs shows a blank, unresponsive screen when loading

You likely have an issue with a change in your config (or it is something I did, please see below for reporting it).

The terminal mode of Emacs will not show errors like the GUI mode. Please start in the GUI mode to get the bug's output.

* the lsp does can not find elements from the modules with a vmlinux and console_commands.json I copied from a similar repo

You need to compile a vmlinux and the modules. A console_commands.json file is just a listing of the compiled files for clangd to look at. It is not a database of tags like a TAGS file is.

# Workflow Tutorials

[Keymaps](./keymaps.md)

[C Userspace Workflow](./c-user-workflow.md)

[Rust Userspace Workflow](./rust-user-workflow.md)

[Linux Kernel Workflow)](./kernel-workflow.md)

[Python Workflow](./python-workflow.md)

# Reporting Issues

Please feel free to open an issue for something you can reproduce on Fedora, with GNOME, in Wayland, on Bash.

Accounts asking for help only for Windows or Mac will be blocked for not reading this. This includes WSL.

Please submit the output of:

```
~/.config/emacs/bin/doom doctor
```

# Credits

All the people who contributed. For Copyright please see the [contributors file.](./contributors.md) 

- [Basic Kernel Install and parts of the code by Stefan Roesch](https://devkernel.io/posts/kernel-dev-setup-editor/)
