
# Introduction

*Yay Evil!* - Doom Emacs mascot.

Doom Emacs is an interesting beast. It overlays the popular Vim modal and key commands as an *evil* mode, a Vim mode, and thus the above joke. Emacs is known for it's Lisp scripting system, but with it's less than conventional keybindings with predate the original IBM PC by over a decade.

We have a mix of Emacs and Vim commands to use, with the Spacemacs style Mnemonic leader key and organization that many Vim bundles have adopted which can make this a difficult beast to give a tutorial on. I will use a mix of Neovim and Emacs commands that I commonly use in my day to day development.

When I write a string of text such `ctrl+s`; I mean you press and hold ctrl, then press s, then release both keys. This strongly differs from the Emacs way of indicating keys such as `C-s` which also how you bind keys in Emacs Lisp. Emacs documentation will also use the notation of `M-x` which stands for holding the Meta key and then pressing x, which is commonly labeled as Alt on modern computers. This is common way to execute functions, including your own custom functions. I will use the notation of `alt+x`.

When you see a string of text with commas as such `SPC, z, l`; it means press the SPC key and release, then z and release, and then l and release.

Much of this documentation is shared from my Neo-elk work which does not use this Emacs notation.

You are encouraged to skim this from start to finish as we explore each new usecase as it comes up and often may not repeat a basic concept.

# What are you expected to know?

How to use GNOME, how to use the Bash command line, adding elements to your Bash config (mostly what file *is* your Bash config and how to add to it), some Linux fundementals, programming fundmentals, some vim basics, some emacs basics, and Git basics.

# Kernel Development

## Setting up the LSP

One of the most helpful tools for a project as large as the Linux kernel is the ability to look up function definitions by it's name and to travel to any used function definition from any block of code that calls it. You have two tools to do this with. The clangd LSP which requires a compile_commands.json file be created from a kernel build or the ctags program which generates a tags file from the source code itself.

A downside to these tools is that they require remaking after changing branches with large differences or after a git pull. One way to work around this between RHEL branches and upstream is to keep seperate repos for each major repo you work on. Example: rhel8 rhel9 upstream_some_subsystem torvalds

Using the LSP is the recommended choice. The LSP has more functionality and is faster with looking up commands. It is slower to initially create with the time with both building the kernel & modules, and the indexer running; these are once in a while operations. A tags file needs to be recreated far more often because the line lookups drift much more easily with any trival changes in the line numbering. There are also issues with tag libraries with new Linux kernels that make the LSP a more reliable (but not perfect) choice. Do note: the LSP relies on compiled objects, so looking up elements for options that are not configured in your build will not show things-this will be a better usecase for ctags and cscope.

### Building the compile_commands.json for the upstream kernel

To set this up you will need the a kernel source tree to build for the LSP, Doom Emacs setup (please see the Readme.md), and the needed packages installed (please see the Readme.md). Go into the root of the kernel repo you want to work in.

You can use the commands below to make a very simple and small kernel:

```bash
THREADS=$(($(nproc)-1)) # need to have total threads -1 to avoid starvation issues with compiler not freeing up memory and causing an out-of-memory-issue
make CC='ccache clang' defconfig
make CC='ccache clang' -j$THREADS # should create a vmlinux in the root of the project
./scripts/clang-tools/gen_compile_commands.py # needs a vmlinux in the root
```

You should have a compile_commands.json file in the root of your kernel repo after these commands. Exit Emacs and reenter, go to any .c or .h file. The LSP will run an indexer that will take a while. This is needed for the LSP commands used later in this file.

You can use either GCC or Clang. Clang will likely compile a kernel faster but has issues with older versions of RHEL and the mainline kernel.

#### Building RHEL kernels on Fedora

Right now only RHEL 8 is tested.

This bundle depends on tools that may not be in the RHEL EPEL repos but are in a workstation focused distribution like Fedora, but you need a RHEL base to build a RHEL kernel.

You can use toolbox as a very quick way to setup a development environment to build a RHEL kernel and use the editor outside the container directly on Fedora.

```bash
sudo dnf install toolbox -y

toolbox create --distro rhel --release 8.8 # download and setup the container, requires a major and minor version number

toolbox enter # enter the container, or, toolbox enter rhel-toolbox-8.8, if you have more than one toolbox
```

Now that you are in the RHEL container you need to install the software used to build a RHEL kernel. A lot of software in Fedora is not available in RHEL unless you install the unofficial EPEL repo. This requires a [free developer subscription to enable from Red Hat.](https://developers.redhat.com/articles/renew-your-red-hat-developer-program-subscription#why_do_we_ask_you_to_re_register_once_a_year_)

```bash
sudo subscription-manager register # Enter your username and password
sudo subscription-manager attach
```

Both will give permission denied errors, but I have not found this to be an issue in actually using the container.

[Enable the EPEL repo for the container.](https://docs.fedoraproject.org/en-US/epel/)

```bash
sudo subscription-manager repos --enable codeready-builder-for-rhel-8-$(arch)-rpms
sudo dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
sudo dnf update --refresh
```

Install the needed pkgs.

```bash
sudo dnf group install "Development Tools"
sudo dnf install ... # everything including and after the clang packages from the readme
```

Generate the RHEL configs.

```bash
make rh-configs
```

Go into redhat/configs and copy one of the config files.

Let us copy the x86-64 real-time config.

```bash
THREADS=$(($(nproc)-1)) # need to have total threads -1 to avoid starvation issues with compiler not freeing up memory and causing an out-of-memory-issue
cp redhat/configs/kernel-rt-4.18.0-x86_64-debug.config .config
# on rhel 8 ccache causes a compiler asm goto error and clang does not work
make olddefconfig
make -j$THREADS
make modules -j$THREADS
./scripts/clang-tools/gen_compile_commands.py
```

- troubleshooting

-- cleaning a repo

You can clean all build artifacts with: 

```bash
make clean && make mrproper
```

-- objtool reports a glibc version not found

```
...rhel8/tools/objtool//fixdep: /lib64/libc.so.6: version `GLIBC_2.33' not found (required by...
```

This may have happened if you tried compiling a RHEL kernel outside the container. Objtool is then linked against glibc. make clean and mrproper run at the root do not clean the tools directory where objtool is built by the compilation process. This is easy to resolve by running the tools specific make clean in the tools dir:

```bash
cd tools && make clean
```

-- compiling (8/old) RHEL kernel on Fedora produces odd error messages

Please use a container to compile RHEL in. See above for details.

#### Building a kernel for CentOS Stream, AlmaLinux, and other distributions on Fedora

This differs in two ways. You need to create and import a container into toolbox since at the time it only has RHEL and Fedora images. The second step is enable the EPEL repo which is slightly different (and easier) for other EL distros.

[Import a container](https://discussion.fedoraproject.org/t/playing-around-with-other-distros-in-toolbox/30081/8)

[Enable EPEL for other EL distros](https://docs.fedoraproject.org/en-US/epel/)

## Navigating Source Code / Studying Source Code

The start of a new section of work for a kernel developer will often involve a call stack with a function to investigate. To investigate we need to activate the LSP. Make sure a vmlinux generated with the kernel instructions was made.

Open Emacs in the kernel root directory:

```
emacs .
```

Navigate to a random .c file to activate the LSP.

A prompt will appear saying this is not part of any project (even if you added the project's root to projects with `SPC, p, a`). Choose the i option to import from the project's detected root. Doom should remember this the next time you select a project from here. 

Note: if you use a subdirectory within a larger git repo the dired in this mini-mode can be fincky. Press `I` to choose interactivemode. You must not use enter, except for the directory you want to select. To go up a directory press escape, to go into a directory, press tab. This may still not work due to an issue with one of the plugins in Doom Emacs.

To demonstrate how quickly this bundle can help you work; navigate to the `task_struct` definition by typing `SPC, z, l` and type in `task_struct`. You should see a list with sections such as Function, Variable, and Struct. You can jump to the Struct section with `ctrl+down arrow`. The first option should be our task_struct; select it with enter.

Stay in normal mode. Move the cursor over the second thread_info and press `shift+k`

```
/*
 * For reasons of header soup (see current_thread_info()), this
 * must be the first element of task_struct.
 */
struct thread_info		thread_info;
[snip]
refcount_t			usage;
```

It will give you the type, which is the same name, and the documentation, which is just the comment above it. Press `q` or `esc` to get out of that pop up.

Not that useful. Move the cursor down over refcount_t and press `shift+k`. It gives you far more information such as it is a struct and an atomic counter. Press `esc` again.

Now let us head to one of the most complicated parts of the kernel. The scheduler with the `schedule` definition. Once again `SPC, z, l` and type in `schedule`. Notice as you type you get a list of options you can press the up and down key to check out the different variants of schedule and their source code.

```
asmlinkage __visible void __sched schedule(void)
{
	struct task_struct *tsk = current;

#ifdef CONFIG_RT_MUTEXES
	lockdep_assert(!tsk->sched_rt_mutex);
#endif

	if (!task_is_running(tsk))
		sched_submit_work(tsk);
	__schedule_loop(SM_NONE);
	sched_update_worker(tsk);
}
EXPORT_SYMBOL(schedule);
```

Move the cursor to be over `task_is_running`. Press `g, d` (goto definition) to navigate to that definition. It is a macro. Press `ctrl+o` to return (you might need to press it twice if you moved your cursor in that function). Move the cursor over `__schedule_loop` and press `g, d` to go to the definition.

If you press `spc, ,` (space and then a comma) you get a list of the open buffers. As you *jump* through the source code Emacs keeps an open buffer for every file you visited/jumped to, included the directory (it has the `Dired by name` category instead of C/\*l category).

If you need to see the last git commits for this file use `SPC, g, F` to get a list of the files (there may be a submenu you need to press q to get out of, use `SPC, g, F` again will get you back in without the submenu). Select an item with up or down keys and press enter to see a window to the right with the details of the commit.

If you need to make note of the file path, use `SPC, f, Y` to get the project file name location or `SPC, f, y` to get the absolute file path to your user home `~`.

If you wish to navigate back to the subdirectory you are in you may use `SPC, w, d`. In this mode the current pane will be replace with a dired file explorer. You can use `^` to go up a directory. `+` to create a directory.

To search project wide for a file use `SPC, f, F` and type in the name to find it. Note: unlike Neo-elk, this does not use fuzzy find to match substrings of the text you type in to search for.

To search project wide for a string of text use `SPC, \` and type in the string of text to find it. Also not a fuzzy find.

To go up a directory in a dired window press the `backspace` key. 

Future research: https://github.com/doomemacs/doomemacs/issues/2768

## Resolving Merge Conflicts

In the kernel root directory:

```bash
emacs .
```

Press `space, .` to search for the file with a merge conflict. Note that if you move the cursor over it and press enter on a directory it will open a bigger dired window to navigate. To keep to the popup window type in the name followed by directory slash to see the next file. Select the file with the merge conflict.

Press `\`, then <<<< to search for the first conflict block.

To quickly jump between sections, say 3 lines down, you can press `3j`, or 3 lines up, `3k`. Notice the relative line numbers to your left.

To go to a specific line number, say line 3, press `3, shift+g`.

Once you have identify a section to delete you can delete quickly, say 5 lines, with the `5, d, d` key combination.

To delete a single line press the `d, d` key combination.

You can select multiple lines visually with a highlight with `shift+v` and the arrow keys up or down to highlight. You can delete or copy text this way as well.

Deleting text puts it in the clipboard. You can paste it elsewhere with the `p` key. To simply copy press the `y` key with highlighted text.

You can select part of a line for deletion by pressing `v` and then moving the cursor over the section you want to delete, then press `x`. You can also use this same combination to replace a single character but with a `r` instead of an x.

To undo, press `:u` or `u` and redo with the `ctrl+r` combination.

To save changes use `ctrl+s` or `SPC, f, s`.

You can use `SPC, g, g` to get the `git status` of the edits you made.

In this status window (magit) move the mouse cursor over one of the changes you made and press tab to see a summary. Pressing enter will take you to the actual line in the changed line in the source code, but hold off on that.

You can also view recent and unpulled commits also with a tab to expand their lists. Move the mouse cursor over one of them and press enter to see a side window to the right to see the details. Press q to close that split.

Press `q` to leave the magit window.

You can check for more merge conflict markers (`<<<<<<`) with `SPC, s, b` to show a popup list of all instances in the buffer.

[Not working right now: To create a branch]

To stage one of the changes move the cursor over it and press `SPC, g, s` to stage the hunk at this point. If you wish to stage the file (git add some_file) use `SPC, g, S`.

To create a commit use `SPC, g, c, c` to fill in the commit details. Use `:q` to cancel this commit operation-it will not exit Doom. 

Staging hunks is very useful for creating a patch series.

Say you staged the wrong hunk by accident. Use `SPC, g, g` again. Move the mouse cursor over the hunk to unstage and press `u` to unstage it. Move the cursor over the hunk and press `s` to stage the correct hunk. To unstage all items press `shift+u` and `shift+s` to stage all elements.

We can also commit in this magit status window. Press `c` to commit. Type in the commit message. Press `ctrl-c`, and then `ctrl-c` again to save the commit.

[More info on Magic-Emacs' *superpower*](https://www.youtube.com/watch?v=X_iX5US1_xE)

# Creating a Patch Set

# Advanced Git Usage

## Amending Author Information

At Red Hat we have to reset the author information for the commits we cherry-pick into the RHEL/Stream GitLab MRs.

You can do that with

spc g g
c -R
a

then commit as normal

# User-space C Project

*to do*

The LSP for your C project will need a compile_commands.json file.

You will need two packages to create it:

```
sudo dnf install clang clang-tools-extra bear
```

Now generate both the .so, the binary, and the `console_commands.json` with bear. Please do not delete any .o files or the binary as `console_commands.json` is simply a list of these files for clangd to read the symbols from.

If your command is not make, replace make with the different command.

```
bear -- make
```

Have you found this, `bear-able`, so far?

gc to comment out code

shift+< or > to shift left or right

Note that using gotos may take you out of the project and into the system library functions.

spc g g , then p to push to master (depends on your branches)

https://sarcasm.github.io/notes/dev/compilation-database.html

# Python Project

Create a file with:

```
ctrl+x, ctrl+f
```

Type in the file name. You will have a new buffer. Make sure to save it with `ctrl+s` to actually create the file.

# End

If you found this guide useful give thanks by donating to the Doom Emacs project:

[GitHub Sponsors](https://github.com/sponsors/hlissner)

[Liberapay or Paypal](https://github.com/doomemacs/doomemacs?tab=readme-ov-file#contribute)

# References

* Projects and Workspaces | Doom Emacs

https://www.youtube.com/watch?v=HRQhYAz3M-U
