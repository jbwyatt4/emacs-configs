#!/usr/bin/env bash

# SPDX-License-Identifier: GPL-3.0-or-later

# I am linked in the subfolders above because not deleting these folders causes first run issues when switching configs

set -euo pipefail

rm -rf ~/.emacs.d # Fedora by default creates this directory and ignores doom
rm -rf ~/.doom.d
rm -rf ~/.config/doom/
mkdir ~/.config/doom/
cp * ~/.config/doom/
#ln -s $(pwd) ~/.doom.d # doom emacs project functionality does not like being linked to a subdirectory of a git repo

echo "I am a bundle for Doom Emacs. I should be run after you install Doom Emacs. Please run '~/.config/emacs/bin/doom sync' afterwards."
echo "Install the symbols font from nerdfonts.com if you see missing characters."
